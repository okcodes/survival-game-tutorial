// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class SurvivalGameTarget : TargetRules
{
    public SurvivalGameTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;

        ExtraModuleNames.AddRange(new[] {"SurvivalGame"});
    }
}