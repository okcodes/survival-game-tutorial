// Fill out your copyright notice in the Description page of Project Settings.


#include "LootableActor.h"
#include "Components/InteractionComponent.h"
#include "Components/InventoryComponent.h"
// #include "Components/StaticMeshComponent.h"
#include "Engine/DataTable.h"
#include "World/ItemSpawn.h"
#include "Items/Item.h"
#include "Player/SurvivalCharacter.h"

#define LOCTEXT_NAMESPACE "LootableActor"

// Sets default values
ALootableActor::ALootableActor()
{
    LootContainerMesh = CreateDefaultSubobject<UStaticMeshComponent>("DasLootContainerMesh");
    SetRootComponent(LootContainerMesh);

    LootInteraction = CreateDefaultSubobject<UInteractionComponent>("DasLootInteraction");
    LootInteraction->InteractableActionText = LOCTEXT("LootActorText", "Loot");
    LootInteraction->InteractableNameText = LOCTEXT("LootActorText", "Chest");
    LootInteraction->SetupAttachment(GetRootComponent());

    Inventory = CreateDefaultSubobject<UInventoryComponent>("DasInventory");
    Inventory->SetCapacity(20);
    Inventory->SetWeightCapacity(80.f);

    LootRolls = FIntPoint(2, 8);

    SetReplicates(true);
}

// Called when the game starts or when spawned
void ALootableActor::BeginPlay()
{
    Super::BeginPlay();

    LootInteraction->OnInteract.AddDynamic(this, &ALootableActor::OnInteract);

    if (HasAuthority() && LootTable)
    {
        TArray<FLootTableRow*> SpawnItems;
        LootTable->GetAllRows("", SpawnItems);

        int32 Rolls = FMath::RandRange(LootRolls.GetMin(), LootRolls.GetMax());

        for (int32 i = 0; i < Rolls; ++i)
        {
            const FLootTableRow* LootRow = SpawnItems[FMath::RandRange(0, SpawnItems.Num() - 1)];

            ensure(LootRow);

            float ProbabilityRoll = FMath::FRandRange(0.f, 1.f);

            while (ProbabilityRoll > LootRow->Probability)
            {
                LootRow = SpawnItems[FMath::RandRange(0, SpawnItems.Num() - 1)];
                ProbabilityRoll = FMath::FRandRange(0.f, 1.f);
            }

            if (LootRow && LootRow->Items.Num())
            {
                for (auto& ItemClass : LootRow->Items)
                {
                    if (ItemClass)
                    {
                        const int32 Quantity = Cast<UItem>(ItemClass->GetDefaultObject())->GetQuantity();
                        Inventory->TryAddItemFromClass(ItemClass, Quantity);
                    }
                }
            }
        }
    }
}

void ALootableActor::OnInteract(ASurvivalCharacter* Character)
{
    if (Character)
    {
        Character->SetLootSource(Inventory);
    }
}

#undef LOCTEXT_NAMESPACE
