// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SurvivalCharacter.generated.h"

// Forward declarations. We only need a first reference with a forward declaration for it to be recognized in the file.
class UCameraComponent;
class UInteractionComponent;
enum class EEquippableSlot : uint8;
class UEquippableItem;
class UGearItem;

USTRUCT()
struct FInteractionData
{
    GENERATED_BODY()

    FInteractionData()
    {
        ViewedInteractionComponent = nullptr;
        LastInteractionCheckTime = 0.f;
        bInteractHeld = false;
    }

    // The current interactable component we're viewing, if there is one.
    UPROPERTY()
    UInteractionComponent* ViewedInteractionComponent;

    // The time when we last checked for an interactable
    UPROPERTY()
    float LastInteractionCheckTime;

    // Whether the local player is holding the interact key
    UPROPERTY()
    bool bInteractHeld;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnEquippedItemsChanged, const EEquippableSlot, Slot,
                                             const UEquippableItem*, Item);

UCLASS()
class SURVIVALGAME_API ASurvivalCharacter : public ACharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ASurvivalCharacter();

    // The mesh to have equipped if we don't have an item equipped - i.e. the bare skin meshes
    UPROPERTY(BlueprintReadOnly, Category = Mesh)
    TMap<EEquippableSlot, USkeletalMesh*> NakedMeshes;

    // The players body meshes.
    UPROPERTY(BlueprintReadOnly, Category = Mesh)
    TMap<EEquippableSlot, USkeletalMeshComponent*> PlayerMeshes;

    /**Our players inventory*/
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components")
    class UInventoryComponent* PlayerInventory;

    /** Interaction component used to allow other players to loot us when we have died */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components")
    class UInteractionComponent* LootPlayerInteraction;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* SpringArmComponent;

    UPROPERTY(EditAnywhere, Category="Components")
    UCameraComponent* CameraComponent;

    UPROPERTY(EditAnywhere, Category="Components")
    USkeletalMeshComponent* HelmetMesh;

    UPROPERTY(EditAnywhere, Category="Components")
    USkeletalMeshComponent* ChestMesh;

    UPROPERTY(EditAnywhere, Category="Components")
    USkeletalMeshComponent* LegsMesh;

    UPROPERTY(EditAnywhere, Category="Components")
    USkeletalMeshComponent* FeetMesh;

    UPROPERTY(EditAnywhere, Category="Components")
    USkeletalMeshComponent* VestMesh;

    UPROPERTY(EditAnywhere, Category="Components")
    USkeletalMeshComponent* HandsMesh;

    UPROPERTY(EditAnywhere, Category="Components")
    USkeletalMeshComponent* BackpackMesh;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    virtual void Restart() override;

    virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

public:
    UFUNCTION(BlueprintCallable)
    void SetLootSource(class UInventoryComponent* NewLootSource);

    UFUNCTION(BlueprintPure, Category = "Looting")
    bool IsLooting() const;

protected:

    // Begin being looted by a player
    UFUNCTION()
    void BeginLootingPlayer(class ASurvivalCharacter* Character);

    UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
    void ServerSetLootSource(class UInventoryComponent* NewLootSource);

    /** The inventory that we are currently looting from. */
    UPROPERTY(ReplicatedUsing = OnRep_LootSource, BlueprintReadOnly)
    UInventoryComponent* LootSource;

    UFUNCTION()
    void OnLootSourceOwnerDestroyed(AActor* DestroyedACtor);

    UFUNCTION()
    void OnRep_LootSource();

public:

    UFUNCTION(BlueprintCallable, Category = "Looting")
    void LootItem(class UItem* ItemToGive);

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerLootItem(class UItem* ItemToLoot);

protected:

    // How often in seconds to check for an interactable object. Set this to zero if you want to check every tick.
    UPROPERTY(EditDefaultsOnly, Category = "Interaction")
    float InteractionCheckFrequency;

    // How far we'll trace when we check if the player is looking at an interactable object
    UPROPERTY(EditDefaultsOnly, Category = "Interaction")
    float InteractionCheckDistance;

    void PerformInteractionCheck();
    void CouldNotFindInteractable();
    void FoundNewInteractable(UInteractionComponent* Interactable);

    void BeginInteract();
    void EndInteract();

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerBeginInteract();

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerEndInteract();

    void Interact();

    // Information about the current state of the players interaction
    UPROPERTY()
    FInteractionData InteractionData;

    FORCEINLINE UInteractionComponent* GetInteractable() const { return InteractionData.ViewedInteractionComponent; }

    FTimerHandle TimerHandle_Interact;

public:

    // True if we're interacting with an item that is an interaction time (for example a lamp that lakes 2 seconds to turn on)
    bool IsInteracting() const;

    // Get the time fill we interact with the current interactable
    float GetRemainingInteractTime() const;

    // Items

    /** [Server] Use an item from our inventory. */
    UFUNCTION(BlueprintCallable, Category = "Items")
    void UseItem(class UItem* Item);

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerUseItem(class UItem* Item);

    /** [Server] Drop an item. */
    UFUNCTION(BlueprintCallable, Category = "Items")
    void DropItem(class UItem* Item, int32 Quantity);

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerDropItem(class UItem* Item, int32 Quantity);

    /** We need this because the pickups use a blueprint base class */
    UPROPERTY(EditDefaultsOnly, Category = "Items")
    TSubclassOf<class APickup> PickupClass;

    bool EquipItem(UEquippableItem* Item);
    bool UnEquipItem(UEquippableItem* Item);

    void EquipGear(UGearItem* Gear);
    void UnEquipGear(EEquippableSlot Slot);

    void EquipWeapon(class UWeaponItem* WeaponItem);
    void UnEquipWeapon();

    UPROPERTY(BlueprintAssignable, Category = "Items")
    FOnEquippedItemsChanged OnEquippedItemsChanged;

    UFUNCTION(BlueprintPure)
    class USkeletalMeshComponent* GetSlotSkeletalMeshComponent(EEquippableSlot Slot);

    UFUNCTION(BlueprintPure)
    FORCEINLINE TMap<EEquippableSlot, UEquippableItem*> GetEquippedItems() const { return EquippedItems; }

    UFUNCTION(BlueprintCallable, Category = "Weapons")
    FORCEINLINE class AWeapon* GetEquippedWeapon() const { return EquippedWeapon; }
protected:

    // Allows for efficient access of equipped items
    UPROPERTY(VisibleAnywhere, Category = "Items")
    TMap<EEquippableSlot, UEquippableItem*> EquippedItems;
    // TODO: Try making the value of the mapping to be of any derived class from UEquippableItem.

    UPROPERTY(ReplicatedUsing = OnRep_Health, BlueprintReadOnly, Category = "Health")
    float Health;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
    float MaxHealth;

public:

    // Modify the players health by either a negative or positive amount. Return the amount of health acutally removed.
    float ModifyHealth(float Delta);

    UFUNCTION()
    void OnRep_Health(float OldHealth);

    UFUNCTION(BlueprintImplementableEvent)
    void OnHealthModified(float HeathDelta);

protected:

    UPROPERTY(VisibleAnywhere, ReplicatedUsing = OnRep_EquippedWeapon)
    class AWeapon* EquippedWeapon;

    UFUNCTION()
    void OnRep_EquippedWeapon();

    void StartFire();
    void StopFire();

    void BeginMeleeAttack();

    UFUNCTION(Server, Reliable)
    void ServerProcessMeleeHit(const FHitResult& MeleeHit);

    UFUNCTION(NetMulticast, Unreliable)
    void MulticastPlayerMeleeFX();
    
    UPROPERTY()
    float LastMeleeAttackTime;

    UPROPERTY(EditDefaultsOnly, Category = Melee)
    float MeleeAttackDistance;

    UPROPERTY(EditDefaultsOnly, Category = Melee)
    float MeleeAttackDamage;

    UPROPERTY(EditDefaultsOnly, Category = Melee)
    class UAnimMontage* MeleeAttackMontage;

    // Called when killed by the player, or killed by something else like the environment.
    void Suicide(struct FDamageEvent const& DamageEvent, const AActor* DamageCauser);
    void KilledByPlayer(struct FDamageEvent const& DamageEvent, class ASurvivalCharacter* Character,
                        const AActor* DamageCauser);

    UPROPERTY(ReplicatedUsing = OnRep_Killer)
    class ASurvivalCharacter* Killer;

    UFUNCTION()
    void OnRep_Killer();

    UFUNCTION(BlueprintImplementableEvent)
    void OnDeath();

    void StartCrouching();
    void StopCrouching();

    // Input actions (executes once) vs Axis actions (not a 0 or 1 value but a gradual input)
    void MoveForward(float Val);
    void MoveRight(float Val);
    void LookUp(float Val);
    void Turn(float Val);

public:
    void StartReload();
    
    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

    UFUNCTION(BlueprintPure)
    // TODO: I don't like this implementation for the is alive stuff. Maybe the player starved to dead and killer might be no-one.
    FORCEINLINE bool IsAlive() const { return Killer == nullptr; }
};
