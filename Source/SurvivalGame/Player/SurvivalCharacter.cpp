// Fill out your copyright notice in the Description page of Project Settings.


#include "SurvivalCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
//#include "Components/SkeletalMeshComponent.h"
#include "Components/InventoryComponent.h"
#include "Player/SurvivalPlayerController.h"
#include "Components/InteractionComponent.h"
#include "World/Pickup.h"
#include "Items/EquippableItem.h"
#include "Items/GearItem.h"
#include "Items/WeaponItem.h"
// #include "Materials/MaterialInstance.h"
#include "SurvivalGame.h"
#include "Items/Item.h"
// #include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/SpringArmComponent.h"
// #include "GameFramework/DamageType.h"
#include "Kismet/GameplayStatics.h"
#include "Weapons/MeleeDamage.h"
#include "Weapons/Weapon.h"
// #include "SurvivalCharacter.h"

#define LOCTEXT_NAMESPACE "SurvivalCharacter"

// Sets default values
ASurvivalCharacter::ASurvivalCharacter()
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
    SpringArmComponent->SetupAttachment(GetMesh(), FName("CameraSocket"));
    SpringArmComponent->TargetArmLength = 0.f;

    CameraComponent = CreateDefaultSubobject<UCameraComponent>("DasCameraComponent");
    CameraComponent->SetupAttachment(SpringArmComponent);
    // All components I create need to be attached to something.
    CameraComponent->bUsePawnControlRotation = true;

    HelmetMesh = PlayerMeshes.Add(EEquippableSlot::EIS_Helmet,
                                  CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DasHelmetMesh")));
    ChestMesh = PlayerMeshes.Add(EEquippableSlot::EIS_Chest,
                                 CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DasChestMesh")));
    LegsMesh = PlayerMeshes.Add(EEquippableSlot::EIS_Legs,
                                CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DasLegsMesh")));
    FeetMesh = PlayerMeshes.Add(EEquippableSlot::EIS_Feet,
                                CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DasFeetMesh")));
    VestMesh = PlayerMeshes.Add(EEquippableSlot::EIS_Vest,
                                CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DasVestMesh")));
    HandsMesh = PlayerMeshes.Add(EEquippableSlot::EIS_Hands,
                                 CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DasHandsMesh")));
    BackpackMesh = PlayerMeshes.Add(EEquippableSlot::EIS_Backpack,
                                    CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DasBackpackMesh")));

    // Tell all the body meshes to use the head mesh for animation
    for (auto& PlayerMesh : PlayerMeshes)
    {
        USkeletalMeshComponent* MeshComponent = PlayerMesh.Value;
        MeshComponent->SetupAttachment(GetMesh());
        MeshComponent->SetMasterPoseComponent(GetMesh());
    }

    PlayerMeshes.Add(EEquippableSlot::EIS_Head, GetMesh());

    // Give the player an inventory with 20 slots, and an 80kg capacity
    PlayerInventory = CreateDefaultSubobject<UInventoryComponent>("PlayerInventory");
    PlayerInventory->SetCapacity(20);
    PlayerInventory->SetWeightCapacity(80.f);

    LootPlayerInteraction = CreateDefaultSubobject<UInteractionComponent>("PlayerInteraction");
    LootPlayerInteraction->InteractableActionText = LOCTEXT("LootPlayerText", "Loot");
    LootPlayerInteraction->InteractableNameText = LOCTEXT("LootPlayerName", "Player");
    LootPlayerInteraction->SetupAttachment(GetRootComponent());
    LootPlayerInteraction->SetActive(false, true);
    LootPlayerInteraction->bAutoActivate = false;

    InteractionCheckFrequency = 0.f;
    InteractionCheckDistance = 1000.f;

    MaxHealth = 100.f;
    Health = MaxHealth;

    GetMesh()->SetOwnerNoSee(true);

    GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
}

// Called when the game starts or when spawned
void ASurvivalCharacter::BeginPlay()
{
    Super::BeginPlay();

    LootPlayerInteraction->OnInteract.AddDynamic(this, &ASurvivalCharacter::BeginLootingPlayer);

    // Try to display the players platform name on their loot card
    if (APlayerState* PS = GetPlayerState())
    {
        LootPlayerInteraction->SetInteractableNameText(FText::FromString(PS->GetPlayerName()));
    }

    // When the player spawns in they have no items equipped, so cache these items (That way, if a player un-equips an item we can set the mesh back to the original mesh).
    for (auto& PlayerMesh : PlayerMeshes)
    {
        NakedMeshes.Add(PlayerMesh.Key, PlayerMesh.Value->SkeletalMesh);
    }
}

void ASurvivalCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(ASurvivalCharacter, LootSource);
    DOREPLIFETIME(ASurvivalCharacter, EquippedWeapon);
    DOREPLIFETIME(ASurvivalCharacter, Killer);

    // If I wanted the player to be covered in blood, or limping or change their animations, I'll need to make this a
    // `DOREPLIFETIME` instead of a `DOREPLIFETIME_CONDITION` otherwise other players won't see those effects.
    DOREPLIFETIME_CONDITION(ASurvivalCharacter, Health, COND_OwnerOnly);
}

bool ASurvivalCharacter::IsInteracting() const
{
    return GetWorldTimerManager().IsTimerActive(TimerHandle_Interact);
}

float ASurvivalCharacter::GetRemainingInteractTime() const
{
    return GetWorldTimerManager().GetTimerRemaining(TimerHandle_Interact);
}

void ASurvivalCharacter::UseItem(class UItem* Item)
{
    // This is the same as calling `!HasAuthority()`.
    if (GetLocalRole() < ROLE_Authority && Item)
    {
        ServerUseItem(Item);
    }

    if (HasAuthority())
    {
        if (PlayerInventory && !PlayerInventory->FindItem(Item))
        {
            return;
        }
    }

    if (Item)
    {
        Item->Use(this);
    }
}

void ASurvivalCharacter::DropItem(class UItem* Item, const int32 Quantity)
{
    if (PlayerInventory && Item && PlayerInventory->FindItem(Item))
    {
        if (GetLocalRole() < ROLE_Authority)
        {
            ServerDropItem(Item, Quantity);
            return;
        }

        if (HasAuthority())
        {
            const int32 ItemQuantity = Item->GetQuantity();
            const int32 DroppedQuantity = PlayerInventory->ConsumeItem(Item, Quantity);

            FActorSpawnParameters SpawnParams;
            SpawnParams.Owner = this;
            SpawnParams.bNoFail = true;
            SpawnParams.SpawnCollisionHandlingOverride =
                ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

            FVector SpawnLocation = GetActorLocation();
            SpawnLocation.Z -= GetCapsuleComponent()->GetScaledCapsuleHalfHeight();

            FTransform SpawnTransform(GetActorRotation(), SpawnLocation);

            ensure(PickupClass);

            APickup* Pickup = GetWorld()->SpawnActor<APickup>(PickupClass, SpawnTransform, SpawnParams);
            Pickup->InitializePickup(Item->GetClass(), DroppedQuantity);
        }
    }
}

bool ASurvivalCharacter::EquipItem(UEquippableItem* Item)
{
    EquippedItems.Add(Item->Slot, Item);
    OnEquippedItemsChanged.Broadcast(Item->Slot, Item);
    return true;
}

bool ASurvivalCharacter::UnEquipItem(UEquippableItem* Item)
{
    if (Item)
    {
        if (EquippedItems.Contains(Item->Slot))
        {
            if (Item == *EquippedItems.Find(Item->Slot))
            {
                EquippedItems.Remove(Item->Slot);
                OnEquippedItemsChanged.Broadcast(Item->Slot, nullptr);
                return true;
            }
        }
    }
    return false;
}

void ASurvivalCharacter::EquipGear(UGearItem* Gear)
{
    if (USkeletalMeshComponent* GearMesh = *PlayerMeshes.Find(Gear->Slot))
    {
        GearMesh->SetSkeletalMesh(Gear->Mesh);
        GearMesh->SetMaterial(GearMesh->GetMaterials().Num() - 1, Gear->MaterialInstance);
    }
}

void ASurvivalCharacter::UnEquipGear(const EEquippableSlot Slot)
{
    if (USkeletalMeshComponent* EquippableMesh = *PlayerMeshes.Find(Slot))
    {
        if (USkeletalMesh* BodyMesh = *NakedMeshes.Find(Slot))
        {
            EquippableMesh->SetSkeletalMesh(BodyMesh);

            // Put the materials back on the body mesh (Since gear may have applied a different material)
            for (int32 i = 0; i < BodyMesh->Materials.Num(); i++)
            {
                if (BodyMesh->Materials.IsValidIndex(i))
                {
                    EquippableMesh->SetMaterial(i, BodyMesh->Materials[i].MaterialInterface);
                }
            }
        }
        else
        {
            // For some gear like backpacks, there is no naked mesh.
            EquippableMesh->SetSkeletalMesh(nullptr);
        }
    }
}

void ASurvivalCharacter::EquipWeapon(UWeaponItem* WeaponItem)
{
    if(WeaponItem && WeaponItem->WeaponClass && HasAuthority())
    {
        if(EquippedWeapon)
        {
            UnEquipWeapon();
        }

        // Spawn the weapon in
        FActorSpawnParameters SpawnParams;
        SpawnParams.bNoFail = true;
        SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
        SpawnParams.Owner = SpawnParams.Instigator = this;

        if(AWeapon* Weapon = GetWorld()->SpawnActor<AWeapon>(WeaponItem->WeaponClass, SpawnParams))
        {
            Weapon->Item = WeaponItem;

            EquippedWeapon = Weapon;
            OnRep_EquippedWeapon();

            Weapon->OnEquip();
        }
    }
}

void ASurvivalCharacter::UnEquipWeapon()
{
    if(HasAuthority() && EquippedWeapon)
    {
        EquippedWeapon->OnUnEquip();
        EquippedWeapon->Destroyed();
        EquippedWeapon = nullptr;
        OnRep_EquippedWeapon();
    }
}

void ASurvivalCharacter::ServerDropItem_Implementation(UItem* Item, const int32 Quantity)
{
    DropItem(Item, Quantity);
}

bool ASurvivalCharacter::ServerDropItem_Validate(UItem* Item, const int32 Quantity)
{
    return true;
}

void ASurvivalCharacter::ServerUseItem_Implementation(UItem* Item)
{
    UseItem(Item);
}

bool ASurvivalCharacter::ServerUseItem_Validate(UItem* Item)
{
    return true;
}

USkeletalMeshComponent* ASurvivalCharacter::GetSlotSkeletalMeshComponent(const EEquippableSlot Slot)
{
    if (PlayerMeshes.Contains(Slot))
    {
        return *PlayerMeshes.Find(Slot);
    }
    return nullptr;
}

float ASurvivalCharacter::ModifyHealth(const float Delta)
{
    const float OldHealth = Health;
    Health = FMath::Clamp<float>(Health + Delta, 0.f, MaxHealth);
    return Health - OldHealth;
}

void ASurvivalCharacter::OnRep_Health(float OldHealth)
{
    OnHealthModified(Health - OldHealth);
}

void ASurvivalCharacter::OnRep_EquippedWeapon()
{
    if(EquippedWeapon)
    {
        EquippedWeapon->OnEquip();
    }
}

void ASurvivalCharacter::StartFire()
{
    if (EquippedWeapon)
    {
        EquippedWeapon->StartFire();
    }
    else
    {
        BeginMeleeAttack();
    }
}

void ASurvivalCharacter::StopFire()
{
    if (EquippedWeapon)
    {
        EquippedWeapon->StopFire();
    }
}

void ASurvivalCharacter::BeginMeleeAttack()
{
    if (GetWorld()->TimeSince(LastMeleeAttackTime) > MeleeAttackMontage->GetPlayLength())
    {
        FHitResult Hit;
        FCollisionShape Shape = FCollisionShape::MakeSphere(15.f);

        FVector StartTrace = CameraComponent->GetComponentLocation();
        FVector EndTrace = (CameraComponent->GetComponentRotation().Vector() * MeleeAttackDistance) + StartTrace;

        FCollisionQueryParams QueryParams = FCollisionQueryParams("MeleeSweep", false, this);

        PlayAnimMontage(MeleeAttackMontage);

        if (GetWorld()->SweepSingleByChannel(Hit, StartTrace, EndTrace, FQuat(), COLLISION_WEAPON, Shape, QueryParams))
        {
            UE_LOG(LogTemp, Warning, TEXT("we hit something with our punch"));
            
            if(ASurvivalCharacter* HitPlayer = Cast<ASurvivalCharacter>(Hit.GetActor()))
            {
                // TODO:
                // if(ASurvivalPlayerController* PC = Cast<ASurvivalPlayerController>(GetController()))
                if(ASurvivalPlayerController* PC = Cast<ASurvivalPlayerController>(HitPlayer->GetController()))
                {
                    PC->OnHitPlayer();
                }
            }
        }

        ServerProcessMeleeHit(Hit);

        LastMeleeAttackTime = GetWorld()->GetTimeSeconds();
    }
}

void ASurvivalCharacter::MulticastPlayerMeleeFX_Implementation()
{
    if(!IsLocallyControlled())
    {
        PlayAnimMontage(MeleeAttackMontage);
    }
}

void ASurvivalCharacter::ServerProcessMeleeHit_Implementation(const FHitResult& MeleeHit)
{
    // BUG: It looks like the punch animation, in the screen of the player that receives the damage is only playing if such player is hit. We might want to move the call to `MulticastPlayerMeleeFX` out of the `MeleeAttackDistance` `if`.
    if(GetWorld()->TimeSince(LastMeleeAttackTime) > MeleeAttackMontage->GetPlayLength() && (GetActorLocation() - MeleeHit.ImpactPoint).Size() <= MeleeAttackDistance)
    {
        MulticastPlayerMeleeFX();
        // Line commented out because we can damage other things other that character.
        // if(ASurvivalCharacter* HitPlayer = Cast<ASurvivalCharacter>(MeleeHit.GetActor()))
        // {
        UGameplayStatics::ApplyPointDamage(MeleeHit.GetActor(), MeleeAttackDamage, (MeleeHit.TraceStart - MeleeHit.TraceEnd).GetSafeNormal(), MeleeHit, GetController(), this, UMeleeDamage::StaticClass());
        // }
    }
    LastMeleeAttackTime = GetWorld()->GetTimeSeconds();
}

void ASurvivalCharacter::Suicide(FDamageEvent const& DamageEvent, const AActor* DamageCauser)
{
    Killer = this;
    OnRep_Killer();
}

void ASurvivalCharacter::KilledByPlayer(FDamageEvent const& DamageEvent,
    ASurvivalCharacter* Character, const AActor* DamageCauser)
{
    Killer = Character;
    OnRep_Killer();
}

void ASurvivalCharacter::OnRep_Killer()
{
    SetLifeSpan(20.f);

    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetMesh()->SetSimulatePhysics(true);
    GetMesh()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
    GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
    // bReplicateMovement = false; // This is private
    // SetReplicatingMovement(false); // TODO: If next line does not work, uncomment this one and comment next one.
    SetReplicateMovement(false);

    // The `TurnOff` function pauses animations, so, let's not use it.
    // TurnOff();

    LootPlayerInteraction->Activate();

    // Un-equip all equipped items so they can be looted
    if (HasAuthority())
    {
        TArray<UEquippableItem*> Equippables;
        EquippedItems.GenerateValueArray(Equippables);

        for (auto& EquippedItem : Equippables)
        {
            EquippedItem->SetEquipped(false);
        }
    }

    if (IsLocallyControlled())
    {
        SpringArmComponent->TargetArmLength = 500;
        bUseControllerRotationPitch = true;

        if (ASurvivalPlayerController* PC = Cast<ASurvivalPlayerController>(GetController()))
        {
            PC->ShowDeathScreen(Killer);
        }
    }
}

void ASurvivalCharacter::StartCrouching()
{
    Crouch();
}

void ASurvivalCharacter::StopCrouching()
{
    UnCrouch();
}

void ASurvivalCharacter::MoveForward(float Val)
{
    if (Val != 0.f)
    {
        AddMovementInput(GetActorForwardVector(), Val);
    }
}

void ASurvivalCharacter::MoveRight(float Val)
{
    if (Val != 0.f)
    {
        AddMovementInput(GetActorRightVector(), Val);
    }
}

void ASurvivalCharacter::LookUp(float Val)
{
    if (Val != 0.f)
    {
        AddControllerPitchInput(Val);
    }
}

void ASurvivalCharacter::Turn(float Val)
{
    if (Val != 0.f)
    {
        AddControllerYawInput(Val);
    }
}

void ASurvivalCharacter::StartReload()
{
    if(EquippedWeapon)
    {
        EquippedWeapon->StartReload();
    }
}

// Called every frame
void ASurvivalCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (!HasAuthority() || IsInteracting())
    {
        if (GetWorld()->TimeSince(InteractionData.LastInteractionCheckTime) > InteractionCheckFrequency)
        {
            PerformInteractionCheck();
        }
    }
}

void ASurvivalCharacter::Restart()
{
    Super::Restart();

    if (ASurvivalPlayerController* PC = Cast<ASurvivalPlayerController>(GetController()))
    {
        PC->ShowInGmeUI();
    }
}

float ASurvivalCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
    AActor* DamageCauser)
{
    Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

    const float DamageDealt = ModifyHealth(-DamageAmount);

    if (Health <= 0.f)
    {
        if(ASurvivalCharacter* KillerCharacter = Cast<ASurvivalCharacter>(DamageCauser->GetOwner()))
        {
            KilledByPlayer(DamageEvent, KillerCharacter, DamageCauser);
        } else
        {
            Suicide(DamageEvent, DamageCauser);
        }
    }

    return DamageDealt;
}

void ASurvivalCharacter::SetLootSource(UInventoryComponent* NewLootSource)
{
    /** If the item we're lootin gets destroyed, we need to tell the client to remove their Loot screen */
    if (NewLootSource && NewLootSource->GetOwner())
    {
        NewLootSource->GetOwner()->OnDestroyed.AddUniqueDynamic(this, &ASurvivalCharacter::OnLootSourceOwnerDestroyed);
    }

    if (HasAuthority())
    {
        if (NewLootSource)
        {
            // Looting a player keeps their body alive for an extra 2 minutes to provide enough time to loot their items
            if (ASurvivalCharacter* Character = Cast<ASurvivalCharacter>(NewLootSource->GetOwner()))
            {
                Character->SetLifeSpan(120.f);
            }
        }

        LootSource = NewLootSource;
    }
    else
    {
        ServerSetLootSource(NewLootSource);
    }
}

bool ASurvivalCharacter::IsLooting() const
{
    return LootSource != nullptr;
}

void ASurvivalCharacter::BeginLootingPlayer(ASurvivalCharacter* Character)
{
    if (Character)
    {
        Character->SetLootSource(PlayerInventory);
    }
}

void ASurvivalCharacter::OnLootSourceOwnerDestroyed(AActor* DestroyedACtor)
{
    // Remove loot source
    if (HasAuthority() && LootSource && DestroyedACtor == LootSource->GetOwner())
    {
        ServerSetLootSource(nullptr);
    }
}

void ASurvivalCharacter::OnRep_LootSource()
{
    // Bring up or remove the looting menu
    if (ASurvivalPlayerController* PC = Cast<ASurvivalPlayerController>(GetController()))
    {
        if (PC->IsLocalController())
        {
            if (LootSource)
            {
                PC->ShowLootMenu(LootSource);
            }
            else
            {
                PC->HideLootMenu();
            }
        }
    }
}

void ASurvivalCharacter::LootItem(UItem* ItemToGive)
{
    if (HasAuthority())
    {
        if (PlayerInventory && LootSource && ItemToGive && LootSource->HasItem(
            ItemToGive->GetClass(), ItemToGive->GetQuantity()))
        {
            const FItemAddResult AddResult = PlayerInventory->TryAddItem(ItemToGive);

            if (AddResult.ActualAmountGiven > 0)
            {
                LootSource->ConsumeItem(ItemToGive, AddResult.ActualAmountGiven);
            }
            else
            {
                // Tell player why the couldn't loot the item
                if (ASurvivalPlayerController* PC = Cast<ASurvivalPlayerController>(GetController()))
                {
                    PC->ClientShowNotification(AddResult.ErrorText);
                }
            }
        }
    }
    else
    {
        ServerLootItem(ItemToGive);
    }
}

void ASurvivalCharacter::ServerLootItem_Implementation(UItem* ItemToLoot)
{
    LootItem(ItemToLoot);
}

bool ASurvivalCharacter::ServerLootItem_Validate(UItem* ItemToLoot)
{
    return true;
}

void ASurvivalCharacter::ServerSetLootSource_Implementation(UInventoryComponent* NewLootSource)
{
    SetLootSource(NewLootSource);
}

bool ASurvivalCharacter::ServerSetLootSource_Validate(UInventoryComponent* NewLootSource)
{
    return true;
}

void ASurvivalCharacter::PerformInteractionCheck()
{
    if (GetController() == nullptr)
    {
        return;
    }

    InteractionData.LastInteractionCheckTime = GetWorld()->GetTimeSeconds();

    // Get location of the player's camera

    FVector EyesLoc;
    FRotator EyesRot;

    GetController()->GetPlayerViewPoint(EyesLoc, EyesRot);

    // Get where the camera is at some point in the distance

    FVector TraceStart = EyesLoc;
    FVector TraceEnd = (EyesRot.Vector() * InteractionCheckDistance) + TraceStart;
    FHitResult TraceHit;

    FCollisionQueryParams QueryParams;
    QueryParams.AddIgnoredActor(this);

    // Cast a line

    if (GetWorld()->LineTraceSingleByChannel(TraceHit, TraceStart, TraceEnd, ECC_Visibility, QueryParams))
    {
        // Check if we hit an interactable object
        if (TraceHit.GetActor())
        {
            // If it hits something with an interaction component on it
            if (UInteractionComponent* InteractionComponent = Cast<UInteractionComponent>(
                TraceHit.GetActor()->GetComponentByClass(UInteractionComponent::StaticClass())))
            {
                float Distance = (TraceStart - TraceHit.ImpactPoint).Size();
                if (InteractionComponent != GetInteractable() && Distance <= InteractionComponent->InteractionDistance)
                {
                    FoundNewInteractable(InteractionComponent);
                }
                else if (Distance > InteractionComponent->InteractionDistance && GetInteractable())
                {
                    CouldNotFindInteractable();
                }

                return;
            }
        }
    }

    CouldNotFindInteractable();
}

void ASurvivalCharacter::CouldNotFindInteractable()
{
    // We've lost focus on an interactable. Clear the timer.
    if (GetWorldTimerManager().IsTimerActive(TimerHandle_Interact))
    {
        GetWorldTimerManager().ClearTimer(TimerHandle_Interact);
    }

    // Tell the interactable we've stopped focusing on it, and clear the current itneractable
    if (UInteractionComponent* Interactable = GetInteractable())
    {
        Interactable->EndFocus(this);

        if (InteractionData.bInteractHeld)
        {
            EndInteract();
        }
    }

    InteractionData.ViewedInteractionComponent = nullptr;
}

void ASurvivalCharacter::FoundNewInteractable(UInteractionComponent* Interactable)
{
    EndInteract();

    if (UInteractionComponent* OldInteractable = GetInteractable())
    {
        OldInteractable->EndFocus(this);
    }

    InteractionData.ViewedInteractionComponent = Interactable;
    Interactable->BeginFocus(this);
}

void ASurvivalCharacter::BeginInteract()
{
    if (!HasAuthority())
    {
        ServerBeginInteract();
    }

    /** As an optimization, the server only checks that we're looking at an item once we begin interacting with it.
     This saves the server doing a check every tick for the interactable Item. The exception is a non-instant interact.
     In this case, the server will check every tick for the duration of th interact.*/
    if (HasAuthority())
    {
        PerformInteractionCheck();
    }

    InteractionData.bInteractHeld = true;

    if (UInteractionComponent* Interactable = GetInteractable())
    {
        Interactable->BeginInteract(this);

        if (FMath::IsNearlyZero(Interactable->InteractionTime))
        {
            Interact();
        }
        else
        {
            GetWorldTimerManager().SetTimer(TimerHandle_Interact, this, &ASurvivalCharacter::Interact,
                                            Interactable->InteractionTime, false);
        }
    }
}

void ASurvivalCharacter::EndInteract()
{
    if (!HasAuthority())
    {
        ServerEndInteract();
    }

    InteractionData.bInteractHeld = false;

    GetWorldTimerManager().ClearTimer(TimerHandle_Interact);

    if (UInteractionComponent* Interactable = GetInteractable())
    {
        Interactable->EndInteract(this);
    }
}

void ASurvivalCharacter::Interact()
{
    GetWorldTimerManager().ClearTimer(TimerHandle_Interact);

    if (UInteractionComponent* Interactable = GetInteractable())
    {
        Interactable->Interact(this);
    }
}

void ASurvivalCharacter::ServerEndInteract_Implementation()
{
    EndInteract();
}

bool ASurvivalCharacter::ServerEndInteract_Validate()
{
    return true;
}

void ASurvivalCharacter::ServerBeginInteract_Implementation()
{
    BeginInteract();
}

bool ASurvivalCharacter::ServerBeginInteract_Validate()
{
    // Returning true means "go ahead with the remote procedure call".
    return true;
}

// Called to bind functionality to input
void ASurvivalCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASurvivalCharacter::StartFire);
    PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASurvivalCharacter::StopFire);

    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
    // Question: If I override this function in this class, and then send the reference to the parent function, not the overriden one, which one gets called?
    PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

    PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ASurvivalCharacter::BeginInteract);
    PlayerInputComponent->BindAction("Interact", IE_Released, this, &ASurvivalCharacter::EndInteract);

    PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASurvivalCharacter::StartCrouching);
    PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASurvivalCharacter::StopCrouching);

    PlayerInputComponent->BindAxis("MoveForward", this, &ASurvivalCharacter::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &ASurvivalCharacter::MoveRight);
}

#undef LOCTEXT_NAMESPACE
