// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponItem.h"

#include "Player/SurvivalCharacter.h"
// #include "Player/SurvivalPlayerController.h"

UWeaponItem::UWeaponItem()
{
}

bool UWeaponItem::Equip(ASurvivalCharacter* Character)
{
    const bool bEquipSuccessful = Super::Equip(Character);

    if (bEquipSuccessful && Character)
    {
        Character->EquipWeapon(this);
    }

    return bEquipSuccessful;
}

bool UWeaponItem::UnEquip(ASurvivalCharacter* Character)
{
    const bool bUnEquipSuccessful = Super::UnEquip(Character);

    if (bUnEquipSuccessful && Character)
    {
        Character->UnEquipWeapon();
    }

    return bUnEquipSuccessful;
}
